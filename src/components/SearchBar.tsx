import { ChangeEvent, useRef, useContext } from 'react';
import { SearchResults } from '.';
import { PlacesContext } from '../context/places/PlacesContext';


export const SearchBar = () => {

  const { searchPlacesByTerm } = useContext(PlacesContext)
  //Debounce normal
  const debounceRef = useRef<NodeJS.Timeout>();

  const onQueryChanged = (e:ChangeEvent<HTMLInputElement>) => {
    if (debounceRef.current) {
      clearTimeout(debounceRef.current);
    }
    debounceRef.current = setTimeout(() => {
      searchPlacesByTerm(e.target.value);
    }, 1000);
  }

  return (
    <div className="search-container">
      <input
        type="text"
        className="form-control"
        placeholder="Buscar lugar.."
        onChange={onQueryChanged}
      />
      <SearchResults />
    </div>
  )
}
