import React from 'react';
import ReactDOM from 'react-dom';
import { MapsApp } from './MapsApp';

import mapboxgl from 'mapbox-gl'; // or "const mapboxgl = require('mapbox-gl');"
 
mapboxgl.accessToken = 'pk.eyJ1IjoibWFyaW84YSIsImEiOiJja3prZ3kwdG8zM3R0Mm9rdXJla2o3cm55In0.2Hrl5vMaUkLkR5h8sI4EYw';

if (!navigator.geolocation) {
  alert('Tu navegador no tiene opcion de geolication');
  throw new Error('Tu navegador no tiene opcion de geolication')
}

ReactDOM.render(
  <React.StrictMode>
    <MapsApp />
  </React.StrictMode>,
  document.getElementById('root')
);