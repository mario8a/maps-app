import { createContext } from "react";
import { Feature } from "../../interfaces/places";
//El contexto es lo que nosotros vamos a mostrar o exponerle a los demas componentes
export interface PlacesContextProps {
  isLoading: boolean;
  userLocation?: [number, number];
  isLoadingPlaces: boolean;
  places: Feature[]
  searchPlacesByTerm: (query: string) => Promise<Feature[]>
}

export const PlacesContext = createContext<PlacesContextProps>({} as PlacesContextProps);