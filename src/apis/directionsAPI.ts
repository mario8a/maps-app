import axios from "axios";

const directionsApi = axios.create({
  baseURL: 'https://api.mapbox.com/directions/v5/mapbox/driving',
  params: {
    alternatives: false,
    geometries: 'geojson',
    overview: 'simplified',
    steps: false,
    access_token: 'pk.eyJ1IjoibWFyaW84YSIsImEiOiJja3prZ3kwdG8zM3R0Mm9rdXJla2o3cm55In0.2Hrl5vMaUkLkR5h8sI4EYw'
  }
});

export default directionsApi;