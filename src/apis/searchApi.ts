import axios from "axios";

const searchApi = axios.create({
  baseURL: 'https://api.mapbox.com/geocoding/v5/mapbox.places',
  params: {
    limit: 5,
    language: 'es',
    access_token: 'pk.eyJ1IjoibWFyaW84YSIsImEiOiJja3prZ3kwdG8zM3R0Mm9rdXJla2o3cm55In0.2Hrl5vMaUkLkR5h8sI4EYw'
  }
});

export default searchApi;